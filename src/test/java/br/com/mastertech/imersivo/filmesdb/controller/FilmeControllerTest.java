package br.com.mastertech.imersivo.filmesdb.controller;

import java.util.List;

import org.assertj.core.util.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.service.FilmeService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { FilmeController.class } )
public class FilmeControllerTest {

	@MockBean
	FilmeService filmeService;

	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void deveObterListaDeFilmes() throws Exception {
		
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setGenero("Drama");
		List<Filme> filmes = Lists.newArrayList(filme);
		
		Mockito.when(filmeService.obterFilmes()).thenReturn(filmes);
		
		//Execute o /get em /filme e a expectativa é que o status esteja ok (200)
		//também espera-se que haja uma string dentro do conteúdo que contenha "1".
		mockMvc.perform(MockMvcRequestBuilders.get("/filme"))
			   .andExpect(MockMvcResultMatchers.status().isOk())
			   .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("1")))
			   .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Drama")));
		
	}
	
	
	@Test
	public void deveCriarUmFilme() throws JsonProcessingException, Exception {
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setTitulo("Crepúsculo");
		
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.post("/filme")
		       .contentType(MediaType.APPLICATION_JSON_UTF8)
		       .content(objectMapper.writeValueAsString(filme)))	
			   .andExpect(MockMvcResultMatchers.status().isCreated());
		
		//essa linha deu erro
		//Mockito.verify(filmeService).criarFilme(filme);
		
		//ArgumentsAreDifferent, fazer oMockito any
		Mockito.verify(filmeService).criarFilme(Mockito.any(Filme.class));

	}
	
	@Test
	public void deveApagarUmFilme() throws JsonProcessingException, Exception {
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setTitulo("Crepúsculo");
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.delete("/filme/1"))
		       //.contentType(MediaType.APPLICATION_JSON_UTF8)
		       //.content(objectMapper.writeValueAsString(filme)))	
			   .andExpect(MockMvcResultMatchers.status().isNoContent());

		Mockito.verify(filmeService).apagarFilme(filme.getId());

	}
	
	
	@Test
	public void deveEditarUmFilme() throws JsonProcessingException, Exception {
		
		long id = 1;
		
		Filme filme = new Filme();
		filme.setId(id);
		filme.setTitulo("Crepúsculo");
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.patch("/filme/1")
		       .contentType(MediaType.APPLICATION_JSON_UTF8)
		       .content(objectMapper.writeValueAsString(filme)))	
			   .andExpect(MockMvcResultMatchers.status().isOk());
		
		Mockito.when(filmeService.editarFilme(id, filme)).thenReturn(filme);

	}
	
}
