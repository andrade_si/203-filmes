package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.DiretorDTO;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.DiretorRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DiretorService.class})
public class DiretorServiceTest {
	@Autowired
	DiretorService diretorService;
	
	@MockBean
	DiretorRepository diretorRepository;
	
	@MockBean
	FilmeService filmeService;
	
	@Test
	public void deveListarTodosOsDiretores() {
		
		//setup
		Diretor diretor = new Diretor();
		List<Diretor> diretores = new ArrayList<>();
		diretores.add(diretor);
		
		Mockito.when(diretorRepository.findAll()).thenReturn(diretores);
		
		//action
		List<Diretor> resultado = Lists.newArrayList(diretorService.obterDiretores());
		
		//check
		Diretor diretorResultado = resultado.get(0);
		
		assertNotNull(diretorResultado);
		assertEquals(diretor, resultado.get(0));
	}
	
	@Test
	public void deveIncluirUmDiretor() {
		
		//setup
		Diretor diretor = new Diretor();
		diretor.setId(1L);
		diretor.setNome("Diretora Maria");
		diretor.setIdade(40);
		
		//action
		diretorService.criarDiretor(diretor);
				
		//check
		Mockito.verify(diretorRepository).save(diretor);
	}
	
	@Test
	public void deveApagarUmDiretor() {
		//setup
		long id = 1;
		Diretor diretor = new Diretor();
		diretor.setId(id);
		diretor.setNome("Diretora Maria");
		diretor.setIdade(40);
		
		Mockito.when(diretorRepository.findById(id)).thenReturn(Optional.of(diretor));
		
		//action
		diretorService.apagarDiretor(id);
		
		//check
		Mockito.verify(diretorRepository).delete(diretor);
		
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarODiretor() {
		//setup
		long id = 1;
		Diretor diretor = new Diretor();
		diretor.setId(id);
		diretor.setNome("Diretora Maria");
		diretor.setIdade(40);
		
		Mockito.when(diretorRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		diretorService.apagarDiretor(id);
	}
	
	@Test
	public void deveObterAListaDeFilmesDeUmDiretor() {
		
		//setup
		long id = 1;
		
		List<FilmeDTO> filmes = new ArrayList<>();
		FilmeDTO filmeDTO = Mockito.mock(FilmeDTO.class);
		filmes.add(filmeDTO);
		
		Mockito.when(filmeService.obterFilmes(id)).thenReturn(filmes);
		
		//ACTION
		List<FilmeDTO> resultado = Lists.newArrayList(diretorService.obterFilmes(id));
			
		//CHECK
		assertEquals(filmeDTO, resultado.get(0));
	}
	
	@Test
	public void deveObterUmDiretorEspecifico() {
		//setup
		DiretorDTO diretorDTO = Mockito.mock(DiretorDTO.class);
		long id = 1;
		
		Mockito.when(diretorRepository.findSummary(id)).thenReturn(diretorDTO);
		
		//action
		List<DiretorDTO> resultado = Lists.newArrayList(diretorService.obterDiretor(id));
		
		//check
		DiretorDTO diretorResultado = resultado.get(0);
		
		assertNotNull(diretorResultado);
		assertEquals(diretorDTO, resultado.get(0));
	}
}
