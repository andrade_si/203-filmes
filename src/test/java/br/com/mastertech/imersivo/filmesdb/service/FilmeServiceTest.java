package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {FilmeService.class})
public class FilmeServiceTest {

	@Autowired
	FilmeService filmeService;
	
	@MockBean
	FilmeRepository filmeRepository;
	
	@Test
	public void deveListarTodosOsFilmes() {
		
		//setup
		Filme filme = new Filme();
		List<Filme> filmes = new ArrayList<>();
		filmes.add(filme);
		
		Mockito.when(filmeRepository.findAll()).thenReturn(filmes);
		
		//action
		List<Filme> resultado = Lists.newArrayList(filmeService.obterFilmes());
		
		//check
		Filme filmeResultado = resultado.get(0);
		
		assertNotNull(filmeResultado);
		assertEquals(filme, resultado.get(0));
	}

	@Test
	public void deveListarTodosOsFilmesDeUmDiretor()
	{
		
		//Criar o retorno esperado (Iterable<FilmeDTO>)
		
		
		//SETUP
		FilmeDTO filmeDTO = Mockito.mock(FilmeDTO.class);
		
		List<FilmeDTO> filmes = new ArrayList<FilmeDTO>();
		filmes.add(filmeDTO);
		
		long id = 1;
		Mockito.when(filmeRepository.findAllByDiretor_Id(id)).thenReturn(filmes);
		
		//ACTION
		List<FilmeDTO> resultado = Lists.newArrayList(filmeService.obterFilmes(id));
	
		//CHECK
		assertEquals(filmeDTO, resultado.get(0));
	}
	
	@Test
	public void devApagarUmFilme() {
		//setup
		long id = 1;
		Filme filmeDoBancoDeDados = new Filme();
		filmeDoBancoDeDados.setId(id);
		filmeDoBancoDeDados.setTitulo("Matrix");
		filmeDoBancoDeDados.setGenero("Ação");
		
		Mockito.when(filmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBancoDeDados));
				
		//action
		filmeService.apagarFilme(id);
		
		//check
		Mockito.verify(filmeRepository).delete(filmeDoBancoDeDados);
		
	}
	
	@Test
	public void deveCriarUmFilme() {
		//setup
		Filme filme = new Filme();	
		
		//action
		filmeService.criarFilme(filme);
		
		//check
		Mockito.verify(filmeRepository).save(filme);
	}
	
	
	@Test
	public void deveEditarUmFilme() {
		//setup
		long id = 1;
		Filme filmeDoBancoDeDados = new Filme();
		filmeDoBancoDeDados.setId(id);
		filmeDoBancoDeDados.setTitulo("Matrix");
		filmeDoBancoDeDados.setGenero("Ação");
		
		String NovoTitulo = "Banana";
		Filme filmeDoFrontEnd = new Filme();
		filmeDoFrontEnd.setId(id);
		filmeDoFrontEnd.setTitulo(NovoTitulo);
		filmeDoFrontEnd.setGenero("Comédia");
		
		Mockito.when(filmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBancoDeDados));
		Mockito.when(filmeRepository.save(filmeDoBancoDeDados)).thenReturn(filmeDoBancoDeDados);
		
		//action
		Filme resultado = filmeService.editarFilme(id, filmeDoFrontEnd);
		
		//check
		
		//Verificar se veio conteúdo na lista
		assertNotNull(resultado);
		
		//Verifica se método foi chamado
		Mockito.verify(filmeRepository).save(filmeDoBancoDeDados);
		
		//Verifica se o gênero foi alterado
		assertEquals("Comédia", resultado.getGenero());
		
		//Verificar se o título não foi alterado
		assertNotEquals(NovoTitulo, resultado.getTitulo());
		
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOFilme() {
		//setup
		long id = 1;
		Filme filmeDoFrontEnd = new Filme();
		filmeDoFrontEnd.setId(id);
		filmeDoFrontEnd.setTitulo("Matrix");
		filmeDoFrontEnd.setGenero("Ação");
		
		Mockito.when(filmeRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		filmeService.editarFilme(id, filmeDoFrontEnd);
	}
}
