package br.com.mastertech.imersivo.filmesdb.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;

@Repository
public interface FilmeRepository extends CrudRepository<Filme, Long>{
	
	Iterable<FilmeDTO> findAllByDiretor_Id(Long id);
}
